// 3. Create a variable getCube and use the exponent operator to compute for the cube of a number. (A cube is any number raised to 3)
let getCube = function(num) {
    return num ** 3;
  };
  console.log(getCube(2));

// 4. Using Template Literals, print out the value of the getCube variable with a message of The cube of <num> is…
console.log(`The cube of 3 is ${getCube(3)}.`);

// 5. Create a variable address with a value of an array containing details of an address.
let address = [  "258 Washington Ave",  "NW",  "California",  "90011"];
console.log(address);

// 6. Destructure the array and print out a message with the full address using Template Literals.
let [street, city, state, zipCode] = address;
 console.log(`I live at ${street}, ${city}, ${state} ${zipCode}.`);

 /*
 // SLIGHT ERROR IN 7 th PART . REST OIF THE CODE IS SHOWING DESIRED OUPUT
 */

// 7. Create a variable animal with a value of an object data type with different animal details as it’s properties.
let animal = {
    Name: "Frankie",
    species: "crocodile",
    weight : 1075,
    height_ft : 20,
    height_in : 3
  };
  console.log(animal);

// 8. Destructure the object and print out a message with the details of the animal using Template Literals.
let {name , species, weight , height } = animal;
console.log(`${Name} was a saltwater ${species}. He weighted ${weight} kgs with a measurement of ${height_ft} ft ${height_in} in.`);

// 9. Create an array of numbers.
let numbers = [1, 2, 3, 4,5];
console.log(numbers);

// 10. Loop through the array using forEach, an arrow function and using the implicit return statement to print out the numbers.
numbers.forEach(num => console.log(num));

// 11. Create a variable reduceNumber and using the reduce array method and an arrow function console log the sum of all the numbers in the array.
let reduceNumber = numbers.reduce((sum, num) => sum + num, 0);
console.log(`${reduceNumber}`);

// 12. Create a class of a Dog and a constructor that will accept a name, age and breed as it’s properties.
let dog = {
    Name: "Frankie",
    age : 5,
    breed : "Miniature Dachshund"
  };
  console.log(dog);